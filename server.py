from flask import Flask, request, render_template
import cv2
import numpy as np
import os
import glob

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['jpg'])

app = Flask(__name__, template_folder='template', static_url_path='/static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config["DEBUG"] = True

import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
import os
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

np.set_printoptions(threshold=sys.maxsize)

def load_images(folder):
    images = np.array([])
    histogram = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))
        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        n, bin_edges = np.histogram(gray, bins=10, range=(0, 256))
        histogram = [x/10000 for x in n]
        if histogram is not None:
            images = np.append([[images]], histogram)
    images = images.reshape(int(len(images)/10), 10)
    return images


imgtrain = load_images('./static/dataset/train/Apple Braeburn')
imgtrain2 = load_images('./static/dataset/train/Apple Golden 1')
imgtrain3 = load_images('./static/dataset/train/Apple Granny Smith')
imgtrain4 = load_images('./static/dataset/train/Apple Golden 2')
imgtrain5 = load_images('./static/dataset/train/Apple Golden 3')
target = []
for i in range(len(imgtrain)):
    target.append(0)
for i in range(len(imgtrain2)):
    target.append(1)
for i in range(len(imgtrain3)):
    target.append(2)
for i in range(len(imgtrain4)):
    target.append(3)
for i in range(len(imgtrain5)):
    target.append(4)

train = np.concatenate((imgtrain, imgtrain2, imgtrain3, imgtrain4, imgtrain5))
neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(train, target)

test_0 = load_images('./static/dataset/test/Apple Braeburn')
test_1 = load_images('./static/dataset/test/Apple Golden 1')
test_2 = load_images('./static/dataset/test/Apple Granny Smith')
test_3 = load_images('./static/dataset/test/Apple Golden 2')
test_4 = load_images('./static/dataset/test/Apple Golden 3')
test = np.concatenate((test_0, test_1, test_2, test_3, test_4))

y_pred = neigh.predict(test)
y_test = []
for i in range(len(test_0)):
    y_test.append(0)
for i in range(len(test_1)):
    y_test.append(1)
for i in range(len(test_2)):
    y_test.append(2)
for i in range(len(test_3)):
    y_test.append(3)
for i in range(len(test_4)):
    y_test.append(4)


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/akurasi', methods=['GET', 'POST'])
def akurasi():
    braeburn = []
    golden1 = []
    golden2 = []
    golden3 = []
    granny = []
    files = glob.glob('static/dataset/test/Apple Braeburn/*.jpg')
    for f in files:
        braeburn.append(f)

    files2 = glob.glob('static/dataset/test/Apple Golden 1/*.jpg')
    for f in files2:
        golden1.append(f)

    files3 = glob.glob('static/dataset/test/Apple Golden 2/*.jpg')
    for f in files3:
        golden2.append(f)

    files4 = glob.glob('static/dataset/test/Apple Golden 3/*.jpg')
    for f in files4:
        golden3.append(f)

    files5 = glob.glob('static/dataset/test/Apple Granny Smith/*.jpg')
    for f in files5:
        granny.append(f)

    from sklearn import metrics
    accuracy = metrics.accuracy_score(y_test,y_pred)
    # presisi = metrics.precision_score(y_test,y_pred)
    # recall = metrics.recall_score(y_test,y_pred)

    hasil = []
    for i in y_test:
        if (i == 0) :
            hasil.append('Apple Braeburn')
        elif (i == 1) :
            hasil.append('Apple Golden 1')
        elif (i == 2) :
            hasil.append('Apple Granny Smith')
        elif (i == 3) :
            hasil.append('Apple Golden 2')
        else :
            hasil.append('Apple Golden 3')
    
    hasil2 = []
    for i in y_pred:
        if (i == 0) :
            hasil2.append('Apple Braeburn')
        elif (i == 1) :
            hasil2.append('Apple Golden 1')
        elif (i == 2) :
            hasil2.append('Apple Granny Smith')
        elif (i == 3) :
            hasil2.append('Apple Golden 2')
        else :
            hasil2.append('Apple Golden 3')

    return render_template('akurasi.html', 
        kelas=y_test,
        hasil=hasil,
        kelas2=y_pred,
        hasil2=hasil2,
        akurasi=accuracy,
        # presisi=presisi,
        # recall=recall,
        braeburn = braeburn,
        golden1 = golden1,
        golden2 = golden2,
        golden3 = golden3,
        granny = granny
    )

@app.route('/datalatih', methods=['GET', 'POST'])
def datalatih():
    braeburn = []
    golden1 = []
    golden2 = []
    golden3 = []
    granny = []
    files = glob.glob('static/dataset/train/Apple Braeburn/*.jpg')
    for f in files:
        braeburn.append(f)

    files2 = glob.glob('static/dataset/train/Apple Golden 1/*.jpg')
    for f in files2:
        golden1.append(f)

    files3 = glob.glob('static/dataset/train/Apple Golden 2/*.jpg')
    for f in files3:
        golden2.append(f)

    files4 = glob.glob('static/dataset/train/Apple Golden 3/*.jpg')
    for f in files4:
        golden3.append(f)

    files5 = glob.glob('static/dataset/train/Apple Granny Smith/*.jpg')
    for f in files5:
        granny.append(f)

    return render_template('datalatih.html', 
        braeburn = braeburn,
        golden1 = golden1,
        golden2 = golden2,
        golden3 = golden3,
        granny = granny
        )

@app.route('/pengujian', methods=['GET', 'POST'])
def pengujian():
    files = glob.glob('static/uploads/*.jpg')
    for f in files:
        os.remove(f)
    return render_template('pengujian.html')


@app.route('/hasil', methods=['GET', 'POST'])
def process():
    if 'file' not in request.files:
        return jsonify({"Hasil": "File tidak ada"})
    if 'file' not in request.files:
        returndata = 'No file part'
        return jsonify({'File': returndata})
    file = request.files['file']
    if file.filename == '':
        returndata = 'File Tidak ada'
        return jsonify({'File': returndata})
    if file:
        images = np.array([])
        histogram = []
        filename = file.filename
        file.save(os.path.join('static/uploads', file.filename))
        img = cv2.imread(os.path.join('static/uploads', filename))
        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        n, bin_edges = np.histogram(gray, bins=10, range=(0, 256))
        for i in n :
            hist = i/10000
            histogram.append(hist)
        if histogram is not None:
            images = np.append([[images]], histogram)
        images = images.reshape(int(len(images)/10), 10)

        hasil = neigh.predict(images)

        if (hasil[0] == 0) :
            hasil = 'Apple Braeburn'
        elif (hasil[0] == 1) :
            hasil = 'Apple Golden 1'
        elif (hasil[0] == 2) :
            hasil = 'Apple Granny Smith'
        elif (hasil[0] == 3) :
            hasil = 'Apple Golden 2'
        else :
            hasil = 'Apple Golden 3'

    return render_template('hasil.html', histogram=images, hasil=hasil, filename=filename)

@app.route('/prepro', methods=['GET', 'POST'])
def prepro():
    img = cv2.imread(os.path.join('static/dataset/train/Apple Braeburn/0_Apple Braeburn.jpg'))
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    plt.imsave('static/grayscale/0_100.jpg',gray,cmap='gray')

    histogram = []
    n, bin_edges = np.histogram(gray, bins=10, range=(0, 256))
    for i in n :
        hist = i/10000
        histogram.append(hist)

    plt.figure()
    plt.title("Grayscale Histogram")
    plt.xlabel("grayscale value")
    plt.ylabel("h")
    plt.xticks(np.arange(0, 255, 25)) # setting the ticks
    plt.xlim([0.0, 255.0])  # <- named arguments do not work here
    plt.ylim([0.0, 0.5])  # <- named arguments do not work here

    plt.plot(bin_edges[0:-1], histogram)  # <- or here
    plt.savefig('static/histogram/0_100.jpg')

    img2 = cv2.imread(os.path.join('static/dataset/train/Apple Golden 1/2_Apple Golden 1.jpg'))
    gray2 = cv2.cvtColor(img2, cv2.COLOR_RGB2GRAY)
    plt.imsave('static/grayscale/1_100.jpg',gray2,cmap='gray')

    histogram2 = []
    n2, bin_edges = np.histogram(gray2, bins=10, range=(0, 256))
    for i in n2 :
        hist = i/10000
        histogram2.append(hist)

    plt.figure()
    plt.title("Grayscale Histogram")
    plt.xlabel("grayscale value")
    plt.ylabel("h")
    plt.xticks(np.arange(0, 255, 25)) # setting the ticks
    plt.xlim([0.0, 255.0])  # <- named arguments do not work here
    plt.ylim([0.0, 0.5])  # <- named arguments do not work here

    plt.plot(bin_edges[0:-1], histogram2)  # <- or here
    plt.savefig('static/histogram/1_100.jpg')

    return render_template('prepro.html', 
    train=train , 
    rgb=img,
    gray=gray ,
    histogram=histogram , 
    n = n , 
    rgb2=img2,
    gray2=gray2 ,
    histogram2=histogram2 , 
    n2 = n2 ,
    imgtrain = imgtrain,
    imgtrain2 = imgtrain2,
    imgtrain3 = imgtrain3,
    imgtrain4 = imgtrain4,
    imgtrain5 = imgtrain5
    )


if __name__ == "__main__":
    app.run(debug=True)
